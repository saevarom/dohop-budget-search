# About

This project is a submission for the Dohop hackathon 2015.

Sometimes you just like to casually browse flights. Maybe your destination is not fixed, you just want to get away! We thought we'd make a concept flight search where you could just click the country you want to fly from and see the possible flights in a certain time period. You can also enter your maximum budget to filter the results.

We think this concept is pretty cool, it brings together our interest in flights and travel with displaying data in a graphical way!

This project can be viewed here: [dohop.overcast.io](http://dohop.overcast.io/)

# Installation

This project is a pure HTML/JS/CSS application, no dependencies needed other than a browser. For convenience, consider running a local server in the project folder like so:

    python -m SimpleHTTPServer

Then navigate to http://localhost:8000/

# Example usage

The user interface consists of a few form inputs, and a world map. The user can click a departure country, adjust their travel period and budget and then search for flights.

![A country is selected by clicking it](http://dohop.overcast.io/screenshots/1.png)

Many international airports exists for most countries, the user must select their departing airport from a list of available airports:

![Selecting the departure airport](http://dohop.overcast.io/screenshots/2.png)

The search button is then clicked and all available flights are drawn as routes on the world map. The destinations within budget are shown as larger green dots, whereas more expensive flights are shown as smaller red dots.

![Flight search results](http://dohop.overcast.io/screenshots/3.png)

Each destination can be hovered to see details such as price and number of flight legs.

![Single destination detail](http://dohop.overcast.io/screenshots/4.png)

The user can continue to adjust their criteria, such as extending the departure date range:

![Extending the date range](http://dohop.overcast.io/screenshots/5.png)

Which gives far more results:

![Lots of flights!](http://dohop.overcast.io/screenshots/6.png)


# Limitations

The flight search results are limited to cached search results from the Dohop search engine. Because of that this will not be a comprehensive flight search engine as is. It merely functions as perhaps an interesting concept for a flight search, if nothing else. 

# Attribution

## Libraries used

* [JQuery](http://jquery.com/)
* [D3.js](http://d3js.org/)
* [topoJSON](https://github.com/mbostock/topojson)
* [DataMaps](http://datamaps.github.io/)
* [Bootstrap](http://getbootstrap.com/)
* [Flat UI](http://designmodo.github.io/Flat-UI/)
* [Font Awesome](http://fortawesome.github.io/Font-Awesome/)

## Data

* Flight information, currencies and airports from the Dohop Livestore API.
* Airport latitude/longitude data acquired from Openflights.org (http://openflights.org/data.html)
* ISO 3166 country code conversions based on Node library [node-i18n-iso-countries](https://github.com/michaelwittig/node-i18n-iso-countries)

# Authors

* Sævar Öfjörð Magnússon 
* Arnar Tumi Þorsteinsson 