var dh = null;
var map = null; 
var countries = null;

function showSpinner(){
	$(".search-overlay").show();
}

function hideSpinner(){
	$(".search-overlay").hide();
}

function extractForm(){
	var r = {
		country: $('#departureAirport').val(),
		departureDate: $('#datePicker1').val(),
		returnDate: $('#datePicker2').val(),
		budget: parseInt($('#budget').val()),
		currency: $('#currency').val(),
		airport: $('#departure').val()
	};
	console.log("Form data:");
	console.log(r);
	return r;
}

$('document').ready(function(){

	var defaultFill = '#bdc3c7',
	    activeFill = '#2accAc',
	    highlightFill = '#1abc9c',
	    locale = 'en',
	    dh = new DohopLiveStore(),
	    data = null,
	    formData = null,
	    countries = null,
	    countries_obj = null,
	    currencies = null,
	    airportSelected=false;

	map = new Datamap({
		//projection: 'mercator',
		element: document.getElementById('container'),
        done: function(datamap) {
            datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
                setActiveCountry(geography.id);
                $('#departureAirport').select2('val', alpha3ToAlpha2(geography.id));
            });
        },
		responsive: true,
	    fills: {
	      defaultFill: defaultFill, //the keys in this object map to the "fillKey" of [data] or [bubbles]
	      'origin': 'blue',
	      'within budget': 'green',
	      'too expensive': 'red'
	    },
	    geographyConfig: {
	    	highlightFillColor: highlightFill
	    },
		arcConfig: {
			strokeColor: '#333',
			strokeWidth: 4,
			arcSharpness: 1,
			animationSpeed: 600
		}
	});
	setActiveCountry('ISL');

    $(window).on('resize', function() {
       map.resize();
    });

    // used to simulate latency
    function sleep(milliseconds) {
		var start = new Date().getTime();
		for (var i = 0; i < 1e7; i++) {
			if ((new Date().getTime() - start) > milliseconds){
			  break;
			}
		}
	}

    $('#search').on('click', function(e){
    	showSpinner();
    	e.preventDefault();

    	formData = extractForm();

    	if(!airportSelected) {

	    	dh.get_locations(locale, countries_obj[formData.country])
	    		.done(function(data){
	    			var locations_data = processLocations(data);
	    			console.log(locations_data);
	    			hideSpinner();

	    			$('#city-selection-modal').modal('show')
	    			.on('shown.bs.modal', function(e){
	    				console.log("shown");
	    				$('#departure').select2({data: locations_data}).select2('val', locations_data[0].children[0].id);
	    			});
	    		});
		} else {
	    	dh.get_lowest_fares_per_country(locale, formData.country, formData.airport, formData.departureDate, formData.returnDate, {'stay': '3-20', 'fare-format': 'full', 'currency': formData.currency})
	    		.done(function(d){

	    			data = d;
	    			processData();

				});

		}

	});

    $('#search2').on('click', function(e){
    	airportSelected = true;
    	$('#city-selection-modal').modal('hide');
    	showSpinner();
    	formData = extractForm();

    	dh.get_lowest_fares_per_country(locale, formData.country, formData.airport, formData.departureDate, formData.returnDate, {'stay': '3-20', 'fare-format': 'full', 'currency': formData.currency})
    		.done(function(d){

    			data = d;
    			processData();

			});
	});

	function processData() {

		console.log(data)

		if (data === null || formData === null) {
			return;
		}

		sleep(300);
		hideSpinner();

		var arcs = [];
		var bubbles = [{
			name: data.airports[formData.airport]["ci_n"],
			latitude: iata_airports[formData.airport].lat,
			longitude: iata_airports[formData.airport].lon,
			radius: 5,
			fillKey: 'origin'
		}];
		data.fares.forEach(function(f){
			//if(f.c == currency) {// && f.n_legs_out == 1) {
				var o = iata_airports[formData.airport];
		        var d = iata_airports[f.b];
		        arcs.push({
					origin: {
						latitude: o.lat,
						longitude: o.lon
					},
					destination: {
						latitude: d.lat,
						longitude: d.lon
					}
				});
				bubbles.push({
					name: data.airports[f.b]["ci_n"],
					latitude: d.lat,
					longitude: d.lon,
					radius: ((f.conv_fare <= formData.budget) ? 10 : 5),
					fillKey: ((f.conv_fare <= formData.budget) ? 'within budget' : 'too expensive'),
					price: f.conv_fare,
					currency: formData.currency,
					legs_out: f.n_legs_out
				});
			//}
		});

		map.arc(arcs, {strokeWidth: 1});
		map.bubbles(bubbles, {
			popupTemplate: function(geo, data) {
				if (data.fillKey === "origin") {
					return "<div class='hoverinfo'>" + data.name + "</div>";	
				} else {
					return "<div class='hoverinfo'>" + data.name + ": " + data.price.toMoney(0, '.', ',') + " " + data.currency + " (" + data.legs_out + " leg(s) out)</div>";
				}
			}
		});

		map.legend();
	}



// var $verticalSlider = $("#vertical-slider");
// if ($verticalSlider.length) {
//   $verticalSlider.slider({
//     min: 1,
//     max: 20,
//     value: 5,
//     orientation: "vertical",
//     range: "min",
//     slide: function(event, ui){
//     	$('#budget').val(ui.value*100);
//     	formData = extractForm();
//     	processData(data, formData);
//     }
//   })
// }

	function processLocations(locations_data){
		var results = [];
		$.each(locations_data.matches, function(i, e){
			var current = {'text': e.name, 'children': []}
			console.log(e);
			if(e.hasOwnProperty('children')) {
				$.each(e.children, function(ii, ee){
					console.log(ee)
					current.children.push({id: ee.airports[0], text: ee.name + " (" + ee.airports[0] + ")" });
				});
			} else if(e.hasOwnProperty('members')) {
				$.each(e.members, function(ii, ee){
					console.log(ee)
					current.children.push({id: ee.airports[0], text: ee.name + " (" + ee.airports[0] + ")" });
				});
			} else {
				current.children.push({id: e.airports[0], text: e.name + " (" + e.airports[0] + ")"});
			}
			// $.each(e.airports, function(ii, ee){
			// 	console.log(ee)
			// 	current.children.push({id: ee, text: "" + ee });
			// });
			results.push(current);
		});

		return results;
	}

	dh.get_locale(locale)
		.done(function(data){
			currencies = data.currencies.map(function(obj){return {id: obj[0], text: obj[0]}});
			countries = data.countries.map(function(obj){return {id: obj[0], text: obj[1]}});
			countries_obj = {};
			countries.forEach(function(e){
				countries_obj[e.id] = e.text;
			});
			$('#currency').select2({data: currencies});
			$('#departureAirport').select2({data: countries})
    			.on("change", function(e) { 
    				console.log("change " + alpha2ToAlpha3(e.val));
					setActiveCountry(alpha2ToAlpha3(e.val));
    			});
		});

	function setActiveCountry(country_code_iso3) {
		$('path.datamaps-subunit.active').attr('class', function(index, classNames){
			return classNames.replace('active');
		});
		$('path.datamaps-subunit.' + country_code_iso3).attr('class', function(index, classNames){
			return classNames + ' active';	
		});
		airportSelected = false;
	}

	/* Datepicker stuff */

	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

	var depDatePicker = $("#datePicker1").datepicker({
		onRender: function(date){
			return date.valueOf() < now.valueOf() ? 'disabled' : '';
		}
	}).on('changeDate', function(ev) {
			if (ev.date.valueOf() > retDatePicker.date.valueOf()) {
				var newDate = new Date(ev.date)
				newDate.setDate(newDate.getDate() + 1);
				retDatePicker.setValue(newDate);
			}
	  		depDatePicker.hide();
	  		$('#datePicker2')[0].focus();
		}).data('datepicker');
	var retDatePicker = $("#datePicker2").datepicker({
	  onRender: function(date) {
	    return date.valueOf() <= depDatePicker.date.valueOf() ? 'disabled' : '';
	  }
	}).on('changeDate', function(ev) {
	  retDatePicker.hide();
	  $('#budget').focus();
	}).data('datepicker');

	var today = new Date();
	depDatePicker.setValue(today);

	var newDate = new Date();
    newDate.setDate(newDate.getDate() + 1);
    retDatePicker.setValue(newDate);

});


