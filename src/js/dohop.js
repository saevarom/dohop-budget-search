var DohopLiveStore = (function() {

	/* 
	 * Basic wrapper for the Dohop LiveStore API
	 */

	var self = null;

	function DohopLiveStore(options){
	    this.base_url = 'http://api.dohop.com/api/v1';
	    self = this;
	}

	DohopLiveStore.prototype.get_lowest_fares_per_country = function(locale, user_country, departure_airport, date_from, date_to, extra){
		return call_api('/livestore/' + locale + '/' + user_country + '/per-country/' + departure_airport + '/' + date_from + '/' + date_to, extra);
	};

	DohopLiveStore.prototype.get_lowest_fares_per_airport = function(locale, user_country, departure_airport, arrival_airports, date_from, date_to, extra){
		return call_api('/livestore/' + locale + '/' + user_country + '/per-country/' + departure_airport + '/' + arrival_airports + '/' + date_from + '/' + date_to, extra);
	};

	DohopLiveStore.prototype.get_language = function(){
		return call_api('/language', {})
	};

	DohopLiveStore.prototype.get_locale = function(locale){
		return call_api('/locale/' + locale, {});
	};

	DohopLiveStore.prototype.get_locations = function(locale, prefix){
		return call_api('/picker/' + locale + '/' + prefix, {})
	};

	function call_api(action, data) {
		var dfd = $.Deferred();
		console.log("Calling API: " + action + ", " + data);
		if (data === undefined)
			data = {};
		data['id'] = 'H4cK3r';
		$.get(self.base_url + action, data, function(data){
			dfd.resolve(data);
		}, 'json');
		return dfd.promise();
	}

	return DohopLiveStore;

})();